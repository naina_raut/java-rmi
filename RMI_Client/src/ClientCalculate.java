
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

public class ClientCalculate { 

	public static void main (String[] args) {
		RMIInterface impCal;
		try {
                        //obtains the stub for the registry by giving the host name
                        Registry reg = LocateRegistry.getRegistry(null, 1099);
                        
                        //lookup on the registry stub to obtain the stub for the remote object
  		        impCal = (RMIInterface)reg.lookup("server");
  		        System.out.println("Server connected");
                        
  		        Scanner c = new Scanner(System.in);
  		        
  		        System.out.println("Enter the numbers and operation to be performed");
  		        String out = c.nextLine();
  		        
  		        String[] data = out.split(" ");
  		        char opr = data[0].charAt(0);
  		        int op1 = Integer.parseInt(data[1]);
  		      	int op2 = Integer.parseInt(data[2]);
  		        
                        //invoke the remote method calculate 
  		        int result = impCal.calculate(opr,op1,op2);
                        
                        //Check the result
	        	if(result == -1)
                        {
                            System.out.println("Please enter a valid operator");	
	        	}
	        	else
	        	{
                            System.out.println("The result is "+result);
	        	}
  		        
                        //call remote exit method to close the server
  		        impCal.exit();
 
			}catch (Exception e) {
				System.out.println("CalculateClient exception: " + e);
				}
		}
}
