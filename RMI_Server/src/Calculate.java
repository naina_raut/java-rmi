

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Calculate extends UnicastRemoteObject implements RMIInterface{

	private static final long serialVersionUID = 1L;
        
        //Define the constructor for the remote object
	public Calculate() throws RemoteException {
		super();
	}
	
	@Override
	public int calculate(char opcode, int op1, int op2)
	{
		switch (opcode)
		{
			case '+':
				return op1 + op2;
			case '-':
				return op1 - op2;
			case '/':
				return op1 / op2;
			case '*':
				return op1 * op2;
			case '%':
				return op1 % op2;
		    default:
		    	return -1;
		}				
	}

	@Override
	public int exit() throws RemoteException {
		// TODO Auto-generated method stub
		try{
	        // Unregister ourself
	        Naming.unbind("server");

	        // Unexport; this will also remove us from the RMI runtime
	        UnicastRemoteObject.unexportObject(this, true);

	        System.out.println("CalculatorServer exiting.");
	    }
	    catch(Exception e){}
		return 0;
	}

}
