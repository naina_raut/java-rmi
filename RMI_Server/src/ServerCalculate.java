

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ServerCalculate {
	
	public static void main (String[] argv) {
		
		   try {
                           //Register the remote object
                           Registry reg = LocateRegistry.createRegistry(1099);
                           
                           //Instantiate a remote object
			   Calculate cal = new Calculate();
                           
                           //Bind this object instance to the name "HelloServer" 
			   Naming.rebind("server", cal);

			   System.out.println("Calculate Server is ready.");
			   }catch (Exception e) {
				   System.out.println("Calculate Server failed: " + e);
				}
		   }
}
