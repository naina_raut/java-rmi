

import java.rmi.RemoteException;

public interface RMIInterface extends java.rmi.Remote{
        
        //calculate funation to get the result of the operation based on user input
	public int calculate (char opcode, int op1, int op2) throws RemoteException; 
        
        //to exit the RMI and shut down the server
	public int exit() throws RemoteException; 
}
